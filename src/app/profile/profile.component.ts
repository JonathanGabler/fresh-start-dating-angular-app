import { Component, OnInit } from '@angular/core';
import { UserProfileModel} from '../models/UserProfileModel';
@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {
    user: UserProfileModel = new UserProfileModel({
        userName: 'test@email.com',
        password: 'fake',
        firstName: 'Steve',
        lastName: 'Riegerix',
        interests: 'web dev',
        state: 'MO'
    });
    constructor() { }

    ngOnInit() {}

}
