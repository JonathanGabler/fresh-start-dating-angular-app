export class UserProfileModel {
    id: string;
    userName: string;
    password: string;
    firstName: string;
    lastName: string;
    profileImage: any;
    interests: string;
    state: string;

    constructor(obj: any = null) {
        if (obj != null) {
            // this.id = obj._id.toString();
            this.userName = obj.userName;
            this.password = obj.password;
            this.firstName = obj.firstName;
            this.lastName = obj.lastName;
            this.profileImage = obj.profileImage;
            this.interests = obj.interests;
            this.state = obj.state;
        }
    }
}
